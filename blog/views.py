# -*- coding: utf-8 -*-
import logging

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import (REDIRECT_FIELD_NAME, login as auth_login,
    logout as auth_logout)
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import (ListView, DetailView, FormView, RedirectView,
    TemplateView, CreateView)

from .forms import AddPostForm
from .models import Post, Blog

log = logging.getLogger(__name__)

class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


class LoginView(FormView):

    success_url = '/'
    form_class = AuthenticationForm
    redirect_field_name = REDIRECT_FIELD_NAME
    template_name = 'registration/login.html'

    @method_decorator(sensitive_post_parameters('password'))
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        request.session.set_test_cookie()
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        auth_login(self.request, form.get_user())
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        redirect_to = self.request.REQUEST.get(self.redirect_field_name)
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = self.success_url
        return redirect_to


class LogoutView(RedirectView):
    url = '/'

    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


class PostListView(ListView):
    template_name = 'index.html'
    context_object_name = 'post_list'
    model = Post


class PostView(DetailView, LoginRequiredMixin):
    model = Post
    template_name = 'post.html'

    def get_context_data(self, **kwargs):
        context = super(PostView, self).get_context_data(**kwargs)
        context['can_mark'] = True
        readed_posts = self.request.user.profile.readed_posts
        if readed_posts:
            if str(self.object.pk) in readed_posts.split(','):
                context['can_mark'] = False
        return context


class BlogView(DetailView):
    template_name = 'blog.html'
    model = Blog

    def get_context_data(self, **kwargs):
        context = super(BlogView, self).get_context_data(**kwargs)
        context['can_subscribe'] = False
        if self.request.user.is_authenticated:
            if self.object.user != self.request.user: #нельзя подписаться на свой блог
                subscribed_blogs = self.request.user.profile.subscribed_blogs
                if subscribed_blogs:
                    context['is_subscribed'] = str(self.object.pk) in subscribed_blogs.split(',')
                    context['can_subscribe'] = True
        return context


class BlogListView(ListView):
    template_name = 'blog_list.html'
    context_object_name = 'blog_list'
    model = Blog


class UserFeedView(TemplateView, LoginRequiredMixin):
    template_name = 'user_feed.html'

    def get_context_data(self, **kwargs):
        if self.request.user.profile.subscribed_blogs:
            user_blogs = self.request.user.profile.subscribed_blogs.split(',')
            kwargs['post_list'] = Post.objects.filter(blog__in=user_blogs)
        return kwargs


class SubscribeView(RedirectView):
    def get(self, request, *args, **kwargs):
        blog_id = request.GET.get('blog_id')
        get_object_or_404(Blog, pk=blog_id)
        if request.user.profile.subscribed_blogs:
            user_blogs = request.user.profile.subscribed_blogs.split(',')
            if blog_id not in user_blogs:
                user_blogs.append(blog_id)
                request.user.profile.subscribed_blogs = u','.join(user_blogs)
        else:
            request.user.profile.subscribed_blogs = u','.join([blog_id])
        request.user.profile.save()
        return redirect(request.META['HTTP_REFERER'])


class UnSubscribeView(RedirectView):
    def get(self, request, *args, **kwargs):
        blog_id = request.GET.get('blog_id')
        blog = get_object_or_404(Blog, pk=blog_id)
        user_blogs = request.user.profile.subscribed_blogs.split(',')
        if blog_id in user_blogs:
            user_blogs.remove(blog_id)
            request.user.profile.subscribed_blogs = ','.join(user_blogs)

            readed_posts = request.user.profile.readed_posts.split(',')
            for post in blog.posts.all():
                if str(post.pk) in readed_posts:
                    readed_posts.remove(str(post.pk))
            request.user.profile.readed_posts = readed_posts
            request.user.profile.save()
        return redirect(request.META['HTTP_REFERER'])


class MarkReadView(RedirectView):
    def get(self, request, *args, **kwargs):
        post_id = request.GET.get('post_id')
        get_object_or_404(Post, pk=post_id)
        if request.user.profile.readed_posts:
            readed_posts = request.user.profile.readed_posts.split(',')
            if post_id not in readed_posts:
                readed_posts.append(post_id)
                request.user.profile.readed_posts = u','.join(readed_posts)
        else:
            request.user.profile.readed_posts = u','.join([post_id])
        request.user.profile.save()
        return redirect(request.META['HTTP_REFERER'])


class MarkUnreadView(RedirectView):
    def get(self, request, *args, **kwargs):
        post_id = request.GET.get('post_id')
        get_object_or_404(Post, pk=post_id)
        if request.user.profile.readed_posts:
            readed_posts = request.user.profile.readed_posts.split(',')
            if post_id in readed_posts:
                readed_posts.remove(post_id)
                request.user.profile.readed_posts = u','.join(readed_posts)
                request.user.profile.save()
        return redirect(request.META['HTTP_REFERER'])


class AddPostView(CreateView):
    form_class = AddPostForm
    template_name = 'add_post.html'
    success_url = '/'
