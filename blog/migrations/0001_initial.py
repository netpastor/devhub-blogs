# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.OneToOneField(verbose_name='\u0410\u0432\u0442\u043e\u0440', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '\u0431\u043b\u043e\u0433',
                'verbose_name_plural': '\u0431\u043b\u043e\u0433\u0438',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('text', models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430')),
                ('blog', models.ForeignKey(related_name='posts', verbose_name='\u0411\u043b\u043e\u0433', to='blog.Blog')),
            ],
            options={
                'ordering': ['-date'],
                'verbose_name': '\u0437\u0430\u043f\u0438\u0441\u044c',
                'verbose_name_plural': '\u0437\u0430\u043f\u0438\u0441\u0438',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subscribed_blogs', models.CommaSeparatedIntegerField(max_length=1024, verbose_name='\u041f\u043e\u0434\u043f\u0438\u0441\u0430\u043d \u043d\u0430 \u0431\u043b\u043e\u0433\u0438', blank=True)),
                ('readed_posts', models.CommaSeparatedIntegerField(max_length=1024, verbose_name='\u041f\u0440\u043e\u0447\u0438\u0442\u0430\u043d\u043d\u044b\u0435 \u0441\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u044f', blank=True)),
                ('user', models.OneToOneField(related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': '\u043f\u0440\u043e\u0444\u0438\u043b\u044c',
                'verbose_name_plural': '\u043f\u0440\u043e\u0444\u0438\u043b\u0438',
            },
        ),
    ]
