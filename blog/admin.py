from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Blog, Post, UserProfile


class BlogPostInline(admin.TabularInline):
    model = Post
    extra = 0


class BlogAdmin(admin.ModelAdmin):
    list_display = ['user']
    inlines = [BlogPostInline]


class PostAdmin(admin.ModelAdmin):
    list_display = ['blog', 'title', 'date']
    date_hierarchy = 'date'


class UserProfileInline(admin.TabularInline):
    model = UserProfile
    extra = 0


class UserNewAdmin(UserAdmin):
    inlines = [UserProfileInline]


admin.site.unregister(User)

for model, model_admin in (
        (User, UserNewAdmin),
        (Blog, BlogAdmin),
        (Post, PostAdmin)
    ):
    admin.site.register(model, model_admin)
