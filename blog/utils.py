# -*- coding: utf-8 -*-
import logging

from django.apps import apps
from django.core.mail import send_mail


def send_message(sender, instance, created, **kwargs):
    if created:
        post=instance
        recipient_list = []
        UserProfile = apps.get_model('blog', 'userprofile')
        for profile in UserProfile.objects.all():
            if str(post.blog.pk) in profile.subscribed_blogs.split(','):
                recipient_list.append(profile.user.email)
                if recipient_list:
                    send_mail(
                        subject=u'В вашей ленте появилась новая запись',
                        message=u'В вашей ленте появилась новая запись http://{}'.format(post.get_absolute_url()),
                        from_email='from@example.com',
                        recipient_list=recipient_list,
                        fail_silently=False
                    )