from django.conf.urls import patterns, url

from .views import (PostView, BlogView, BlogListView, LoginView, LogoutView,
                    UserFeedView, SubscribeView, UnSubscribeView, MarkReadView, MarkUnreadView,
                    AddPostView)


urlpatterns = patterns('',
    url(r'^$', BlogListView.as_view(), name='index'),
    url(r'^post/(?P<pk>\d+)/$', PostView.as_view(), name='post_view'),
    url(r'^blog/(?P<pk>\d+)/$', BlogView.as_view(), name='blog_view'),
    url(r'^blog_list/$', BlogListView.as_view(), name='blog_list'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^user_feed/$', UserFeedView.as_view(), name='user_feed'),
    url(r'^subscribe/$', SubscribeView.as_view(), name='subscribe'),
    url(r'^unsubscribe/$', UnSubscribeView.as_view(), name='unsubscribe'),
    url(r'^mark_read/$', MarkReadView.as_view(), name='mark_read'),
    url(r'^mark_unread/$', MarkUnreadView.as_view(), name='mark_unread'),
    url(r'^add_post/$', AddPostView.as_view(), name='add_post'),
)
