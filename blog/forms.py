# -*- coding: utf-8 -*-
from django import forms

from .models import Post

class AddPostForm(forms.ModelForm):
    class Meta(object):
        model = Post
        exclude = ('date',)
