# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import post_delete, post_save

from .utils import send_message


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')
    subscribed_blogs = models.CommaSeparatedIntegerField(u'Подписан на блоги',
        blank=True, max_length=1024)
    readed_posts = models.CommaSeparatedIntegerField(u'Прочитанные сообщения',
        blank=True, max_length=1024)

    class Meta:
        verbose_name = u'профиль'
        verbose_name_plural = u'профили'

    def __unicode__(self):
        return self.user.username


class Blog(models.Model):
    user = models.OneToOneField(User, verbose_name=u'Автор',
        related_name='blog')

    class Meta:
        verbose_name = u'блог'
        verbose_name_plural = u'блоги'

    def __unicode__(self):
        return u'Блог {}'.format(self.user.username)

    def post_count(self):
        return self.posts.count()


class Post(models.Model):
    blog = models.ForeignKey(Blog, related_name='posts', verbose_name=u'Блог')
    title = models.CharField(u'Заголовок', max_length=255)
    text = models.TextField(u'Текст')
    date = models.DateTimeField(u'Дата', auto_now_add=True)

    class Meta:
        verbose_name = u'запись'
        verbose_name_plural = u'записи'
        ordering = ['-date']

    def __unicode__(self):
        return u'Запись {}'.format(self.title)

    def get_absolute_url(self):
        return u'{}{}'.format(
            Site.objects.get_current(),
            reverse('post_view', args=[str(self.pk)])
        )


def create_user(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)
        Blog.objects.create(user=instance)
post_save.connect(create_user, sender=User)

post_save.connect(send_message, sender=Post)
